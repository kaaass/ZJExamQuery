package net.kaaass.tools.gradequery;

import java.util.Arrays;

/**
 * 记录分数
 * 自动生成的JavaBean
 */
public class Mark {
    /**
     * 身份证号
     */
    String id;
    /**
     * 准考证号
     */
    String examId;
    /**
     * 姓名
     */
    String name;
    /**
     * 报名序号
     */
    String reqId;

    /**
     * 等第，顺序：语数英物化生政史地技
     */
    String[] level;
    /**
     * 赋分，顺序：英物化生政史地技
     */
    String[] mark;

    public Mark(String id, String examId, String name, String[] level, String[] mark) {
        this.id = id;
        this.examId = examId;
        this.name = name;
        this.level = level;
        this.mark = mark;
    }

    public Mark(String id, String examId) {
        this.id = id;
        this.examId = examId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getLevel() {
        return level;
    }

    public void setLevel(String[] level) {
        this.level = level;
    }

    public String[] getMark() {
        return mark;
    }

    public void setMark(String[] mark) {
        this.mark = mark;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    @Override
    public String toString() {
        return "Mark{" +
                "name='" + name + '\'' +
                ", level=" + Arrays.toString(level) +
                ", mark=" + Arrays.toString(mark) +
                '}';
    }
}

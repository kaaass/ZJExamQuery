package net.kaaass.tools.gradequery.util;

import javafx.util.Pair;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {
    public static HSSFWorkbook readExcel(File file) throws IOException {
        POIFSFileSystem fileSystem = new POIFSFileSystem(file, true);
        return new HSSFWorkbook(fileSystem);
    }

    public static List<Pair<String, String>> readIn(File file, String sheet, short idC, short eIdC, int st, int ed) throws IOException {
        List<Pair<String, String>> result = new ArrayList<>();
        HSSFWorkbook wb = readExcel(file);
        HSSFSheet hssfSheet = wb.getSheet(sheet);
        HSSFRow row;
        for (int i = st; i < ed + 1; i++) {
            row = hssfSheet.getRow(i);
            HSSFCell cell1 = row.getCell(idC);
            cell1.setCellType(HSSFCell.CELL_TYPE_STRING);
            HSSFCell cell2 = row.getCell(eIdC);
            cell2.setCellType(HSSFCell.CELL_TYPE_STRING);
            String id = cell1.getStringCellValue();
            String eId = cell2.getStringCellValue();
            result.add(new Pair<>(id, eId));
            System.out.format("Data read in: %s, %s\n", id, eId);
        }
        return result;
    }
}

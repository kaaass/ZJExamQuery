package net.kaaass.tools.gradequery.util;

import org.apache.poi.hssf.util.CellReference;

import java.util.Random;

public class StringUtil {
    public static String SET_NUM = "0123456789";
    public static String SET_ENG = "abcdefghijklmnopqrstuvwxyz";
    public static String SET_ENG_CAP = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * 随机生成字符串
     *
     * @param len     字符串长度
     * @param dataSet 随机数据内容
     * @return
     */
    public static String randStr(int len, String dataSet) {
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < len; i++) {
            int number = random.nextInt(dataSet.length());
            stringBuilder.append(dataSet.charAt(number));
        }
        return stringBuilder.toString();
    }

    public static short colToShort (String col) {
        return new CellReference(col + "1").getCol();
    }
}
